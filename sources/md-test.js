/* global FIXTURE, TEST, TEST_ASYNC */
var NI = require('tkjs-ni');
var MD = require('./md');

FIXTURE("MD", function() {
  TEST("parse the hello world's front matter", function() {
    var r = MD.parseFrontSync(__dirname + '/md/HelloWorld.md')
    NI.log("Parsed: %J", r);
    NI.assert.has(r, "title");
    NI.assert.has(r, "__content");
  });

  TEST("parse the hello world's front matter only", function() {
    var r = MD.parseFrontSync(__dirname + '/md/HelloWorld.md', true)
    NI.log("Parsed: %J", r);
    NI.assert.hasnt(r, "__content");
  });

  TEST_ASYNC("ASYNC parse the hello world's front matter", function(aDone) {
    return MD.parseFront(__dirname + '/md/HelloWorld.md').then(function(aResult) {
      NI.log("Parsed: %J", aResult);
      NI.assert.has(aResult, "title");
      NI.assert.has(aResult, "__content");
      aDone();
    });
  });

  TEST_ASYNC("ASYNC parse the hello world's front matter only", function(aDone) {
    return MD.parseFront(__dirname + '/md/HelloWorld.md', true).then(function(aResult) {
      NI.log("Parsed: %J", aResult);
      NI.assert.hasnt(aResult, "__content");
      aDone();
    }).catch(aDone);
  });

  TEST("parse and render hello world's markdown", function() {
    var r = MD.parseAndRenderMarkdownSync(__dirname + '/md/HelloWorld.md')
    NI.log("Parsed and rendered: %J", r);
    NI.assert.has(r, "title");
    NI.assert.has(r, "__content");
    NI.assert.has(r, "__html");
  });
});
