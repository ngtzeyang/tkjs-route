/* global __BACKEND__ */
var __MARKED = undefined;
function renderMarkdown(aSource) {
  if (!__MARKED) {
    __MARKED = require('./marked');
    __MARKED.setOptions({
      renderer: new __MARKED.Renderer(),
      gfm: true,
      tables: true,
      breaks: true,
      pedantic: false,
      sanitize: false,
      smartLists: true,
      smartypants: false
    });
  }
  return __MARKED(aSource);
}
exports.renderMarkdown = renderMarkdown;

//----------------------------------------------------------------------
// Server
//----------------------------------------------------------------------
if (__BACKEND__) { (function() {
  var NI = require('tkjs-ni');
  var PATH = require('tkjs-ni/sources/path');
  var FS = require('tkjs-ni/sources/fs');
  var ROUTE = require('./route');
  var useCache = global.isProduction ? true : false;
  NI.log("Markdown pages useCache: %K", useCache);

  var _mds = [];
  global._mds = _mds;

  var frontParser = {
    result: {},
    state: 'firstline',
    frontOnly: false,
    startContent: function(aInitialContent) {
      if (this.frontOnly) {
        this.state = 'done';
      }
      else {
        this.state = 'content';
        this.result.__content = aInitialContent || "";
      }
    },
    parseLine: function(aLine) {
      switch (this.state) {
      case 'firstline': {
        var l = NI.stringTrim(aLine);
        if (NI.stringStartsWith(l,"---")) {
          this.state = 'front';
        }
        else {
          this.startContent(l);
        }
        break;
      }
      case 'front': {
        var l = NI.stringTrim(aLine);
        if (NI.stringStartsWith(l,"---")) {
          this.startContent();
        }
        else {
          var key = NI.stringTrim(NI.stringBefore(l, ":"));
          var val = NI.stringTrim(NI.stringAfter(l, ":"));
          if (!NI.isEmpty(key)) {
            switch(key) {
            case "acl": {
              if (val[0] == "[") {
                this.result[key] = NI.jsonParseArray(val,undefined);
              }
              else {
                this.result[key] = val;
              }
              break;
            }
            default: {
              this.result[key] = val;
              break;
            }
            }
          }
        }
        break;
      }
      case 'content': {
        this.result.__content += aLine + "\n";
        break;
      }
      case 'done': {
        break;
      }
      default: {
        throw NI.format("Internal Error, invalid state: " + this.state);
      }
      }
    },
    end: function(aFilePath) {
      if (!NI.isEmpty(aFilePath)) {
        this.result.filePath = aFilePath;
      }
      return this.result
    }
  };

  function createFrontParser(aFrontOnly) {
    var r = NI.deepClone(frontParser);
    r.frontOnly = aFrontOnly;
    return r;
  }
  exports.createFrontParser = createFrontParser;

  function parseFront(aPath,aFrontOnly) {
    return NI.Promise(function(aResolved,aRejected) {
      var parser = createFrontParser(aFrontOnly);
      try {
        FS.readLines(
          aPath,
          function(aErr) {
            if (aErr) {
              NI.log("ERR: " + aErr)
              aRejected(aErr);
            }
            else {
              aResolved(parser.result);
            }
          },
          function(aLine) {
            parser.parseLine(aLine);
            if (parser.state === 'done') {
              aResolved(parser.end(aPath));
              return false;
            }
            else {
              return true;
            }
          });
      }
      catch (e) {
        NI.log("ERR2: " + e)
        aRejected(e);
      }
    })
  }
  exports.parseFront = parseFront;

  function parseFrontSync(aPath,aFrontOnly) {
    var parser = createFrontParser(aFrontOnly);
    var lines = FS.readFileSync(aPath,'utf8').split(/\r?\n/);
    var numLines = NI.size(lines);
    for (var i = 0; i < numLines; ++i) {
      parser.parseLine(lines[i]);
      if (parser.state === 'done') {
        break;
      }
    }
    return parser.end(aPath);
  }
  exports.parseFrontSync = parseFrontSync;

  function parseAndRenderMarkdownSync(aFile) {
    var r = parseFrontSync(aFile);
    r.__html = renderMarkdown(r.__content);
    return r;
  }
  exports.parseAndRenderMarkdownSync = parseAndRenderMarkdownSync;

  function getMd(aCompOrName) {
    if (typeof aCompOrName === "string") {
      var md = _mds[aCompOrName];
      // NI.log("... getMd(%s): %s", aCompOrName, md?"cached":"loading");
      if (!md) {
        md = parseAndRenderMarkdownSync(aCompOrName);
        if (!md.className) {
          md.className = NI.stringRBefore(PATH.basename(aCompOrName),'.');
        }
        if (!md.url) {
          var path;
          if (NI.stringContains(md.filePath,'/md/')) {
            path = NI.stringBefore(NI.stringAfter(md.filePath,'/md/'),".md");
          }
          else {
            path = md.className;
          }
          md.url = '/md/'+path;
        }
        md = NI.assignClone({
          title: md.title || NI.stringSpaceize(md.className),
          className: md.className,
          props: {},
          serverSide: md.serverSide,
          clientSide: md.clientSide
        }, md);
        if (useCache) {
          _mds[aCompOrName] = md;
        }

      }
      return md;
    }
    else {
      NI.assert.isObject(aCompOrName);
      return aCompOrName;
    }
  }

  function getMdPageParams(aMdParams) {
    var md = getMd(aMdParams);
    NI.assert.isObject(md);
    NI.assert.isString(md.__content);
    if (!md.__html) {
      md.__html = renderMarkdown(md.__content);
    }
    NI.assert.isString(md.__html);
    var layoutPath = (md.layout) ? (md.layout + '.html.ejs') : 'md.html.ejs';
    return {
      layoutPath: layoutPath,
      md: md
    };
  }

  function GET_MD(aBaseName, aMdParams) {
    var pageParams = getMdPageParams(aMdParams);
    ROUTE.GET_PAGE(
      aBaseName, pageParams.layoutPath,
      useCache ?
        pageParams :
        function() {
          return getMdPageParams(aMdParams)
        });
  }

  function registerMdRoutes(aFiles) {
    var files = aFiles || FS.walkSync(PATH.join(__dirname, 'md'));
    // NI.println("... registerMdRoutes: %K", files);
    var files = NI.isString(aFiles) ? FS.walkSync(aFiles) : aFiles;
    NI.forEach(files, function(filePath) {
      var md = getMd(filePath);
      NI.log("MD: Registering '%s', url: '%s', acl: %s, file: '%s'.",
             md.className,
             md.url,
             md.acl,
             md.filePath);
      if (NI.isEmpty(md.url)) {
        throw NI.Error("MD no url defined for '%s'.", filePath);
      }
      if (!NI.isEmpty(md.acl)) {
        ROUTE.ACL.registerACL(md.url, md.acl);
      }
      GET_MD(
        md.url,
        useCache ? md : filePath);
    });
  }
  exports.registerMdRoutes = registerMdRoutes;

})(); }
