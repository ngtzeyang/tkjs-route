/* global FIXTURE, TEST */
var NI = require('tkjs-ni');
var SAFE_COOKIE = require('./safe_cookie');

FIXTURE("SAFE_COOKIE", function() {
  TEST("test object", function() {
    var cookieOptions = {
      secret: "206A4B67_74EE_4115_8B4C_227617651887",
      cookieName: "my-cookie"
    };

    var cookie = {
      foo: "bar"
    };

    var encodedCookie = SAFE_COOKIE.encode(cookieOptions, cookie);
    NI.print("... encodedCookie: %k", encodedCookie);
    var decodedCookie = SAFE_COOKIE.decode(cookieOptions, encodedCookie);
    NI.print("... decodedCookie: %k", decodedCookie);
    NI.assert.isObject(decodedCookie);
    NI.assert.equals("bar", decodedCookie.content.foo);
  })

  TEST("test UUID", function() {
    var cookieOptions = {
      secret: "206A4B67_74EE_4115_8B4C_227617651887",
      cookieName: "my-cookie"
    };

    var encodedCookie = SAFE_COOKIE.encode(cookieOptions, "6DC69CF6_F13A_473A_AABC_F24EB5EED113");
    NI.print("... encodedCookie: %k", encodedCookie);
    var decodedCookie = SAFE_COOKIE.decode(cookieOptions, encodedCookie);
    NI.print("... decodedCookie: %k", decodedCookie);
    NI.assert.isObject(decodedCookie);
    NI.assert.equals("6DC69CF6_F13A_473A_AABC_F24EB5EED113", decodedCookie.content);
  })
})
